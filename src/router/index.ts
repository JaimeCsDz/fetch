import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import LoginView from '../views/ISesionView.vue'
import VistaSesion from '../views/holaView.vue'
import VistaRegistro from '../views/RegistroView.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/isesion',
      name: 'Login',
      component: LoginView
    },
    {
      path: '/hola',
      name: 'Sesion',
      component: VistaSesion
    },
    {
      path: '/registro',
      name: 'Registro',
      component: VistaRegistro
    }
  ]
})

export default router
